---
layout: page
title: "Projects"
---

I'm currently working on several projects:

* [Raft](https://github.com/hoverbear/raft)
* [Gathering Our Voices](https://github.com/BCAAFC/Gathering-Our-Voices)
* Revamping OpenRC -- *(No repo yet!)*
* [SCRUM'2015](http://scrum2015.cs.uvic.ca/)
* [Untangling the Web](http://webhome.cs.uvic.ca/~ycoady/utw/)
